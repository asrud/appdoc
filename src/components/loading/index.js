//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import { colors } from '../../utils';

// create a component
const Loading = () => {
    return (
        <View style={styles.container}>
            <ActivityIndicator size="large" color="#ffffff" />
            <Text style={styles.text}>Loading...</Text>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        width: '100%',
        height: '100%',
        backgroundColor: colors.loadingBackground
    },
    text: {
        fontSize: 18,
        color: colors.text.third, 
        fontWeight: '600',
        marginTop: 16
    }
});

//make this component available to the app
export default Loading;
