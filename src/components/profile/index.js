//import liraries
import React from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { IconRemovePhoto } from '../../assets';
import { colors } from '../../utils';

// create a component
const Profile = ({name, desc, removePhoto, photo, onPress}) => {
    return (
        <View style={styles.container}>
            {!removePhoto && (
                <View style={styles.borderProfile} >
                    <Image style={styles.avatar} source={photo} />               
                </View>
            )}
            {removePhoto && (
                <TouchableOpacity style={styles.borderProfile} onPress={onPress} >
                    <Image style={styles.avatar} source={photo} />
                    {removePhoto && <IconRemovePhoto style={styles.removePhoto}/>}
                </TouchableOpacity>
            )}
            {name && (
                <View>
                    <Text style={styles.name}>{name}</Text>
                    <Text style={styles.profession}>{desc}</Text>
                </View>
            )}            
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20
    },
    borderProfile: {
        width: 130,
        height: 130,
        borderRadius: 130/2,
        borderColor: colors.border,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1
    },
    avatar: {
        width: 110, 
        height: 110,
        borderRadius: 110/2
    },
    name: {
        fontSize: 20,
        fontWeight: '600',
        color: colors.text.primary,
        marginTop: 2,
        textAlign: 'center'
    },
    profession: {
        fontSize: 16,
        fontWeight: '600',
        color: colors.text.secondary,
        marginTop: 2,
        textAlign: 'center'
    },
    removePhoto: {
        position: 'absolute',
        right: 8,
        bottom: 8
    }
});

//make this component available to the app
export default Profile;
