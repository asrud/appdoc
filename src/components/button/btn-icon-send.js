//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { IconSendDark, IconSendLight } from '../../assets';
import { colors } from '../../utils';

// create a component
const Btniconsend = ({disable, onPress}) => {
    if(disable) {
        return (
            <View style={styles.container(disable)}>
                <IconSendDark />
            </View>
        )
    } else {
        return (
            <TouchableOpacity style={styles.container(disable)} onPress={onPress}>
                <IconSendLight />
            </TouchableOpacity>
        );
    }
    
};

// define your styles
const styles = StyleSheet.create({
    container: (disable) => ({
        backgroundColor: disable ? colors.disable : colors.tertiary,
        width: 45,
        height: 45,
        borderRadius: 10,
        paddingTop: 3,
        paddingRight: 3,
        paddingBottom: 8,
        paddingLeft: 8
    }),
});

//make this component available to the app
export default Btniconsend;
