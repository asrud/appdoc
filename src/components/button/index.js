//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { colors } from '../../utils';
import Btniconsend from './btn-icon-send';

// create a component
const Button = ({type, title, onPress, disable}) => {
    if(type === 'btn-icon-send') {
        return <Btniconsend disable={disable} onPress={onPress} />
    }
    if(disable) {
        return (
            <View style={styles.disableBg}>
                <Text style={styles.disableText}>{title}</Text>
            </View>
        );
    }
    return (
        <TouchableOpacity style={styles.container(type)} onPress={onPress}>
            <Text style={styles.text(type)}>{title}</Text>
        </TouchableOpacity>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: (type) => ({
        backgroundColor: type === 'secondary' ? colors.button.secondary.background : colors.button.primary.background ,
        paddingVertical: 10,
        borderRadius: 10    
    }),
    text : (type) => ({
        fontSize: 16,
        fontWeight: '600',
        textAlign: 'center',
        color: type === 'secondary' ? colors.button.secondary.text : colors.button.primary.text ,
    }),
    disableBg: {
        backgroundColor: colors.button.disable.background,
        paddingVertical: 10,
        borderRadius: 10    
    },
    disableText: {
        fontSize: 16,
        fontWeight: '600',
        textAlign: 'center',
        color: colors.button.disable.text
    }
});

//make this component available to the app
export default Button;
