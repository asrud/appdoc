//import liraries
import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { IconDoctor, IconDoctorActive, IconHospitals, IconHospitalsActive, IconMessages, IconMessagesActive } from '../../assets';
import { colors } from '../../utils';

// create a component
const TabItem = ({title, active, onLongPress, onPress}) => {
    const Icon = () => {
        if(title === 'Doctor') {
            return active ? <IconDoctorActive /> : <IconDoctor />
        }
        if(title === 'Messages') {
            return active ? <IconMessagesActive /> :<IconMessages />
        }
        if(title === 'Hospitals') {
            return active ? <IconHospitalsActive /> : <IconHospitals />
        }
        return <IconDoctor />
    }
    return (
        <TouchableOpacity style={styles.container} onPress={onPress} onLongPress={onLongPress}>
            <Icon />
            <Text style={styles.text(active)}>{title}</Text>
        </TouchableOpacity>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: (active) => ({
        fontSize: 10, 
        color:  active ? colors.text.menuactive : colors.text.menuinactive,
        fontWeight: '600',
        marginTop: 4
    })
});

//make this component available to the app
export default TabItem;
