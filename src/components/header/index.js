//import liraries
import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { IconBackDark, IconBackLight } from '../../assets';
import { colors } from '../../utils';

// create a component
const Header = ({title, onPress, type, profile, desc, photo}) => {
    return (
        <View style={styles.container(type)}>
            <TouchableOpacity onPress={onPress}>
                {type === 'dark' ? <IconBackLight /> : <IconBackDark /> }
            </TouchableOpacity>
            <View style={styles.wrapper}>
                <Text style={styles.text(type)}>{title}</Text>
                {profile === "true" && <Text style={styles.desc}>{desc}</Text>}
            </View>
            {profile === "true" && <Image source={photo} style={styles.avatar} /> }
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: (type) => ({
        paddingHorizontal: 16,
        paddingVertical: 30,
        backgroundColor: type === 'dark' ? colors.secondary : colors.background,
        flexDirection: 'row',
        alignItems: 'center',
        elevation: 5,
        marginBottom: 20,
        borderBottomLeftRadius: type === 'dark' ? 20 : 0,
        borderBottomRightRadius: type === 'dark' ? 20 : 0,
        alignItems: 'center'
    }),
    wrapper: {
        flex: 1
    },
    text: (type) => ({
        textAlign: 'center',
        paddingRight: 24,
        fontSize: 20,
        color: type === 'dark' ? colors.text.third : colors.text.primary
    }),
    desc: {
        fontSize: 12,
        textAlign: 'center',
        color: colors.text.subTitle,
        paddingRight: 24,
    }, 
    avatar: {
        height: 46,
        width: 46,
        borderRadius: 46 / 2
    }
});

//make this component available to the app
export default Header;
