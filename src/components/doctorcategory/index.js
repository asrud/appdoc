 //import liraries
import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { ILCatObat, ILCatPsikiater, ILCatUmum } from '../../assets';
import { colors } from '../../utils';

// create a component
const DoctorCategory = ({category, onPress}) => {
    const Icon = () => {
        if(category === 'Dokter Umum') {
            return <ILCatUmum style={styles.illustration}/>
        }
        if(category === 'Psikiater') {
            return <ILCatPsikiater style={styles.illustration}/>
        }
        if(category === 'Dokter Obat') {
            return <ILCatObat style={styles.illustration}/>
        }
        return <ILCatUmum style={styles.illustration}/>
    }
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Icon />
            <Text style={styles.label}>Saya Butuh</Text>
            <Text style={styles.category}>{category}</Text>
        </TouchableOpacity>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        padding: 12,
        backgroundColor: colors.cardlight,
        alignSelf: 'flex-start',
        borderRadius: 10,
        marginRight: 10,
        height: 130
    },
    label: {
        fontSize: 12,
        fontWeight: '300',
        color: colors.text.primary,
        marginTop: 25
    },
    category: {
        fontSize: 12,
        fontWeight: '600',
        color: colors.text.primary
    },

});

//make this component available to the app
export default DoctorCategory;
