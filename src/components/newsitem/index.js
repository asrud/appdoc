//import liraries
import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { colors } from '../../utils';

// create a component
const NewsItem = ({title, date, image}) => {
    return (
        <View style={styles.container}>
            <View style={styles.titleWrapper}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.date}>{date}</Text>
            </View>
            <Image source={{uri: image}} style={styles.image} />   
        </View>
    );s
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: colors.border,
        paddingBottom: 12,
        paddingTop: 16,
        paddingHorizontal: 15
    },
    titleWrapper: {
        flex: 1
    },  
    title: {
        fontSize: 16, 
        fontWeight: '600',
        color: colors.text.primary
    },
    date: {
        fontSize: 12,
        color: colors.text.secondary,
        marginTop: 12
    },
    image: {
        width: 80,
        height: 60, 
        borderRadius: 11
    }
});

//make this component available to the app
export default NewsItem;
