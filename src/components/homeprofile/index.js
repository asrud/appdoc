//import liraries
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import {DummyUser, ILNullPhoto} from '../../assets'
import { colors, getData } from '../../utils';
// create a component
const HomeProfile = ({onPress}) => {
    const [profile, setProfile] = useState({
        photo: ILNullPhoto,
        fullName: '',
        profession: ''
    })

    useEffect(() => {
        getData('user').then(res => {
            const data = res;
            data.photo = {uri : res.photo};
            setProfile(res)
        })
    })
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Image source={profile.photo} style={styles.avatar} />
            <View>
            <Text style={styles.name}>{profile.fullName}</Text>
            <Text style={styles.profession}>{profile.profession}</Text>
            </View>
        </TouchableOpacity>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row'
    },
    avatar: {
        width: 46,
        height: 46,
        borderRadius: 46 / 2,
        marginRight: 20
    },
    name: {
        fontSize: 16, 
        fontWeight: '600',
        color: colors.text.primary,
        textTransform: 'capitalize'
    },
    profession: {
        fontSize: 12,
        fontWeight: '400',
        color: colors.text.secondary,
        textTransform: 'capitalize'
    }
});

//make this component available to the app
export default HomeProfile;
