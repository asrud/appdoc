//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { DummyHospital1 } from '../../assets';
import { colors } from '../../utils/colors';

// create a component
const ListHospitals = ({type, name, address, pic}) => {
    return (
        <View style={styles.container}>
            <Image source={pic} style={styles.pic} />
            <View>
                <Text style={styles.title}>{type}</Text>
                <Text style={styles.title}>{name}</Text>
                <Text style={styles.address}>{address}</Text>
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 16,
        borderBottomWidth: 1,
        borderBottomColor: colors.border
    },
    pic: {
        height: 60,
        width: 80,
        borderRadius: 11,
        marginRight: 16
    },
    title: {
        fontSize: 16,
        fontWeight: '600',
        color: colors.text.primary
    },
    address: {
        fontSize: 12,
        fontWeight: '300',
        color: colors.text.secondary
    }
});

//make this component available to the app
export default ListHospitals;
