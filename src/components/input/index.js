//import liraries
import React from 'react';
import { useState } from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import { colors } from '../../utils';

// create a component
const Input = ({label, value, onChangeText, secureTextEntry, disable, onPressIn}) => {
    const [border, setBorder] = useState(colors.border)
    const onFocusForm = () => {
        setBorder(colors.tertiary)
    }
    const onBlurForm = () => {
        setBorder(colors.border)
    }
    const rudi = () => {
        console.log('masuk')
    }

    return (
        <View style={styles.container}>
            <Text style={styles.label}>{label}</Text>
            <TextInput 
                onFocus={onFocusForm} 
                onBlur={onBlurForm} 
                style={styles.input(border)} 
                value={value}
                onChangeText={onChangeText}
                secureTextEntry={secureTextEntry}
                editable={!disable}
                onPressIn={rudi}
                selectTextOnFocus={!disable}
            />
        </View>
    );
}

// define your styles
const styles = StyleSheet.create({
    container: {
        marginBottom: 20
    },
    input: (border) => (
        {
            borderWidth: 1,
            borderColor: border,
            borderRadius: 10,
            padding: 12
        }
    ),
    label: {
        fontSize: 16, 
        color: colors.text.secondary,
        marginBottom: 6, 
    }
});

//make this component available to the app
export default Input;
