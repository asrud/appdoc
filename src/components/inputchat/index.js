//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import { Button } from '..';
import { colors } from '../../utils';

// create a component
const InputChat = ({value, onChangeText, onButtonPress}) => {
    return (
        <View style={styles.container}>
            <TextInput style={styles.input} placeholder="tulis pesan untuk nairobi" value={value} onChangeText={onChangeText}/>
            <Button disable={value.length < 1} title="send" type="btn-icon-send" onPress={onButtonPress} /> 
        </View>
    );
};

// define your styles 
const styles = StyleSheet.create({
    container: {
        padding: 16,
        flexDirection: 'row',
        backgroundColor: colors.background
    },
    input: {
        backgroundColor: colors.disable,
        padding: 14,
        borderRadius: 10,
        flex: 1,
        marginRight: 10,
        fontSize: 14,
        maxHeight: 45
    }
});

//make this component available to the app
export default InputChat;
