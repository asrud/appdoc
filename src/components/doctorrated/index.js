//import liraries
import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { IconStar } from '../../assets';
import { colors } from '../../utils';

// create a component
const DoctorRated = ({onPress, name, desc, avatar}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Image source={avatar} style={styles.avatar} />
            <View style={styles.profile}>
                <Text>{name}</Text>
                <Text>{desc}</Text>
            </View>
            <View style={styles.rate}>
                <IconStar />
                <IconStar />
                <IconStar />
                <IconStar />
                <IconStar />
            </View>
        </TouchableOpacity>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 16,
        alignItems: 'center'
    },
    avatar: {
        width: 50,
        height: 50, 
        borderRadius: 50 / 2,
        marginRight: 12
    },
    profile: {
        flex: 1
    },  
    name: {
        fontSize: 16,
        fontWeight: '600',
        color: colors.text.primary
    },
    category: {
        fontSize: 12,
        fontWeight: 'normal',
        color: colors.text.secondary,
        marginTop: 2
    },
    rate: {
        flexDirection: 'row'
    }
});

//make this component available to the app
export default DoctorRated;
