//import liraries
//import liraries
import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { colors } from '../../utils';

// create a component
const Link = ({title, size, align, onPress}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Text style={styles.text(size, align)}>{title}</Text>
        </TouchableOpacity>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
    },
    text: (size, align) => ({
        fontSize: size,
        color: colors.text.secondary,
        textDecorationLine: 'underline',
        textAlign: align 
    })
});

//make this component available to the app
export default Link;
