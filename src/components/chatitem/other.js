//import liraries
import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { colors } from '../../utils';

// create a component
const Other = ({text, date, photo}) => {
    return (
        <View style={styles.container}>
            <Image source={photo} style={styles.avatar} />
            <View>
                <View style={styles.chatContent}>
                    <Text style={styles.text}>{text}</Text>
                </View>
                <Text style={styles.date}>{date}</Text>
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        marginBottom: 20,
        alignItems: 'flex-end',
        paddingLeft: 16,
        flexDirection: 'row'
    },
    avatar: {
        width: 30,
        height: 30,
        borderRadius: 30 / 2,
        marginRight: 12
    },
    chatContent: {
        maxWidth: '80%',
        padding: 12, 
        paddingLeft: 18,
        backgroundColor: colors.primary,
        borderRadius: 10,
        borderBottomLeftRadius: 0,
    },
    text: {
        fontSize: 14,
        color: colors.text.third
    },
    date: {
        fontSize: 11,
        color: colors.text.secondary,
        marginTop: 8
    }
});

//make this component available to the app
export default Other;
