//import liraries
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { colors } from '../../utils';

// create a component
const Me = ({text, date}) => {
    return (
        <View style={styles.container}>
            <View style={styles.chatContent}>
                <Text style={styles.text}>{text}</Text>
            </View>
            <Text style={styles.date}>{date}</Text>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        marginBottom: 20,
        alignItems: 'flex-end',
        paddingRight: 16
    },
    chatContent: {
        maxWidth: '70%',
        padding: 12, 
        paddingRight: 18,
        backgroundColor: colors.cardlight,
        borderRadius: 10,
        borderBottomRightRadius: 0,
    },
    text: {
        fontSize: 14,
        color: colors.text.primary
    },
    date: {
        fontSize: 11,
        color: colors.text.secondary,
        marginTop: 8
    }
});

//make this component available to the app
export default Me;
