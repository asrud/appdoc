//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { colors } from '../../utils';
import Me from './me';
import Other from './other';

// create a component
const ChatItem = ({me, text, date, photo}) => {
    if(me) {
        return <Me text={text} date={date}/>
    }
    return <Other text={text} date={date} photo={photo} />
};


//make this component available to the app
export default ChatItem;
