//import liraries
import React from 'react';
import { ImageBackground, StyleSheet, Text, View } from 'react-native';
import { ILGetStarted, ILLogo } from '../../assets';
import { Button } from '../../components';
import { colors } from '../../utils';

// create a component
const GetStarted = ({navigation}) => {
    return (
        <ImageBackground source={ILGetStarted} style={styles.container}>
            <View>
                <ILLogo />
                <Text style={styles.title}>Konsultasi dengan dokter lewat aplikasi lebih mudah dan fleksibel</Text>
            </View>
            <View>
                <Button title='Get Started' onPress={() => navigation.navigate('Register')}/>
                <View style={{height: 16}} />
                <Button type='secondary' title='Masuk' onPress={() => navigation.replace('Login')}/>
            </View>
        </ImageBackground>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        padding: 40,
        flex: 1,
        justifyContent: 'space-between'
    },
    title : {
        fontSize: 28,
        fontWeight: '600',
        marginTop: 90,
        color: colors.text.third
    }
});

//make this component available to the app
export default GetStarted;
