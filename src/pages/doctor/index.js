//import liraries
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { DoctorCategory, HomeProfile, DoctorRated, NewsItem } from '../../components';
import { colors, errorMessage } from '../../utils';
import { Fire } from '../../config';

// create a component
const Doctor = ({navigation}) => {
    const [news, setNews] = useState([]);
    const [categorDoctor, setCategorDoctor] = useState([]);
    const [doctors, setDoctors] = useState([]);
    useEffect(() => {
        let isCancelled = false;
        getNews();
        getTopRatedDoctor();
        getCategoryDoctor();
    }, [])

    
    const getTopRatedDoctor = () => {
        Fire.database().ref('doctors/').orderByChild('rate').limitToLast(3).once('value').then(res => {
            if(res.val()) {
                const data = []
                Object.keys(res.val()).map(key => {
                    data.push({
                        id: key,
                        data: res.val()[key]
                    })
                })
                setDoctors(data)
            }
        }).catch(err => {
            errorMessage(err.message)
        })
        return () => {
            isCancelled = true;
        }
    }

    const getCategoryDoctor = () => {
        Fire.database().ref('category_doctor/').once('value').then(res => {
            if(res.val()) {
                const data = res.val();
                const filterData = data.filter(el => el !== null)
                setCategorDoctor(filterData)
            }
        }).catch(err => {
            errorMessage(err.message)
        })
        return () => {
            isCancelled = true;
        }
    }

    const getNews = () => {
        Fire.database().ref('news/').once('value').then(res => {
            if(res.val()) {
                const data = res.val();
                const filterData = data.filter(el => el !== null)
                setNews(filterData)
            }
        }).catch(err => {
            errorMessage(err.message)
        })
        return () => {
            isCancelled = true;
        }
    }

    return (
        <View style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.topContent}>
                    <View style={{height: 30}} />
                    <HomeProfile onPress={() => navigation.navigate('UserProfile')}/>
                    <Text style={styles.welcome}>Mau konsultasi dengan siapa hari ini?</Text>
                    <View style={styles.wrapperscroll}>
                        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                            <View style={styles.category}>
                                <View style={{width:15}} />
                                {
                                    categorDoctor.map(item => {
                                        return <DoctorCategory key={item.id} category={item.category} onPress={() => navigation.navigate('SelectDoctor', item)}/>
                                    })
                                }
                                <View style={{width:5}} />
                            </View>
                        </ScrollView>
                    </View>
                    <Text style={styles.sectionlabel}>Dokter Rating Teratas</Text>
                    {doctors.map(doctor => {
                        return <DoctorRated key={doctor.id} name={doctor.data.fullName} desc={doctor.data.profession} avatar={{uri: doctor.data.photo}} onPress={() => navigation.navigate('DoctorProfile', doctor)} />
                
                    })}
                    <Text style={styles.sectionlabel}>Berita Baru</Text>
                </View>
                {news.map(item => {
                    return(
                        <NewsItem key={item.id} title={item.title} date={item.date} image={item.image} />
                    )
                })}
            </ScrollView>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.background
    },
    topContent : {
        paddingHorizontal: 15
    },
    welcome: {
        fontSize: 20,
        fontWeight: '600',
        color: colors.text.primary,
        marginTop: 30,
        marginBottom: 15,
        maxWidth: 250
    },
    category: {
        flexDirection: 'row'
    },
    wrapperscroll: {
        marginHorizontal: -15
    },
    sectionlabel: {
        fontSize: 16, 
        color: colors.text.primary,
        fontWeight: '600',
        marginTop: 30,
        marginBottom: 15
    }
});

//make this component available to the app
export default Doctor;
