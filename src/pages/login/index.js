//import liraries
import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { useDispatch } from 'react-redux';
import { ILLogo } from '../../assets';
import { Button, Input, Link } from '../../components';
import { Fire } from '../../config';
import { colors, errorMessage, storeData, useForm } from '../../utils';

// create a component
const Login = ({navigation}) => {
const dispatch = useDispatch();
const [form, setForm] = useForm({
    email: '',
    password: ''
})


const login = () => {
    dispatch({type: 'SET_LOADING', value: true })
    Fire.auth().signInWithEmailAndPassword(form.email, form.password)
    .then(res => {
        console.log(res)
        dispatch({type: 'SET_LOADING', value: false })
        Fire.database()
        .ref(`users/${res.user.uid}/`)            
        .once('value')
        .then(resDB => {
            if(resDB.val()) {
                storeData('user', resDB.val())
                navigation.replace('MainApp')
            }
        })
    })
    .catch(err =>{
        dispatch({type: 'SET_LOADING', value: false })
        errorMessage(err.message);
    })
}
return (
    <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
            <ILLogo />
            <Text style={styles.title}>Masuk Dan Mulai Berkonsultasi</Text>
            <Input label="Alamat Email" value={form.email} onChangeText={value => setForm('email', value)} />
            <Input label="Password" value={form.password} onChangeText={value => setForm('password', value)} secureTextEntry />
            <Link title="Lupa password" size={12}/>
            <View style={{height: 40}} />
            <Button title="Masuk" onPress={login} />
            <View style={{height: 30}} />
            <Link title="Buat akun baru" size={16} align="center" onPress={() => navigation.navigate('Register')} />
        </ScrollView>
    </View>
);
};

// define your styles
const styles = StyleSheet.create({
container: {
    flex: 1,
    padding: 40,
},
title: {
    fontSize: 20,
    color: colors.text.primary,
    marginVertical: 40,
    maxWidth: 180

}
});

//make this component available to the app
export default Login;
