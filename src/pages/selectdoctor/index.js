//import liraries
import React, { Component, useEffect, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Header, List } from '../../components';
import { Fire } from '../../config';
import { colors } from '../../utils';

// create a component
const SelectDoctor = ({navigation, route}) => {
    const [listDoctor, setListDoctor] = useState([]);
    const itemCategory = route.params;
    useEffect(() => {
        callDoctorByCategory(itemCategory.category.toLowerCase())
    }, [])

    const callDoctorByCategory = (category) => {
        Fire.database().ref('doctors/').orderByChild('category').equalTo(category).once('value').then(res => {
            if(res.val()) {
                const data = [];
                Object.keys(res.val()).map(item => {
                    data.push({
                        id: item,
                        data: res.val()[item]
                    })
                })
                setListDoctor(data);
            }
        })
    }
    return (
        <View style={styles.container}>
            <Header title={`Pilih ${itemCategory.category}`} type="dark" onPress={() => navigation.goBack()} />
            {listDoctor.map(doctor => {
                return (
                    <List key={doctor.id} onPress={()=> navigation.navigate('DoctorProfile', doctor)} type="next" profile={{uri: doctor.data.photo}} name={doctor.data.fullName} desc={doctor.data.gender} />
                )
            })}
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.background
    },
});

//make this component available to the app
export default SelectDoctor;
