//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Button, Header, Profile, ProfileItem } from '../../components';
import { colors } from '../../utils';

// create a component
const DoctorProfile = ({navigation, route}) => {
    const dataDoctor = route.params;
    return (
        <View style={styles.container}>
            <Header title="Doctor Profile" onPress={() => navigation.goBack()} />
            <Profile name={dataDoctor.data.fullName} desc={dataDoctor.data.profession} photo={{uri: dataDoctor.data.photo}}/>
            <ProfileItem label="Alumnus" value={dataDoctor.data.university} />
            <ProfileItem label="Tempat Praktik" value={dataDoctor.data.hospital_address} />
            <ProfileItem label="No. STR" value={dataDoctor.data.str_number} />
            <View style={styles.action}>
                <Button title="Start Consultation" onPress={() => navigation.navigate('Chatting', dataDoctor.data)}/>
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {        
        flex: 1,
        backgroundColor: colors.background
    },
    action: {
        paddingHorizontal: 40,
        paddingTop: 23
    }
});

//make this component available to the app
export default DoctorProfile;
