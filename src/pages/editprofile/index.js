//import liraries
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, ScrollView, ImagePickerIOS } from 'react-native';
import { Button, Header, Input, Profile } from '../../components';
import { colors, getData, storeData } from '../../utils';
import { Fire } from '../../config';
import { showMessage } from 'react-native-flash-message';
import { launchImageLibrary } from 'react-native-image-picker';
import { ILNullPhoto } from '../../assets';


// create a component
const EditProfile = ({navigation}) => {
    const [profile, setProfile] = useState({
        fullName: '',   
        profession: '',
        email: '',
    })
    const [password, setPassword] = useState('')
    const [photo, setPhoto] = useState(ILNullPhoto)
    const [photoForDB, setPhotoForDB] = useState('')
    useEffect(() => {
        if(profile.fullName < 1 ) {
            getData('user').then(res => {
                const data = res;
                setPhoto({uri: res.photo})
                setPhotoForDB(data.photo)
                setProfile(data)
            })
        }
    })
    const update = () => {
       if(password.length > 0) {
            if(password.length < 6) {
                showMessage({
                    message: 'password kurang dari 6 karakter',
                    type: 'default',
                    backgroundColor: colors.error,
                    color: colors.text.third
                })
            } else {
                updatePassword();
                updateProfileData();
            }
       } else {
           updateProfileData();
       }
    }
    const updatePassword = () => {
        Fire.auth().onAuthStateChanged(user => {
            if(user) {
                user.updatePassword(password).catch(err => {
                    showMessage({
                        message: err.message,
                        type: 'default',
                        backgroundColor: colors.error,
                        color: colors.text.third
                    })
                })
            }
        })
    }

    const updateProfileData = () => {
        const data = profile
        data.photo = photoForDB
        Fire.database().ref(`users/${profile.uid}/`).update(data).then(() => {
            showMessage({
                message: 'data berhasil diubah',
                type: 'default',
                backgroundColor: colors.primary,
                color: colors.text.third
            })
            storeData('user', data)
        }).catch(err => {
            showMessage({
                message: err.message,
                type: 'default',
                backgroundColor: colors.error,
                color: colors.text.third
            })
        })
        navigation.replace('MainApp')
    }
    const changeText = (key, value) => {
        setProfile({
            ...profile,
            [key]: value,
        })
    }
    const getImage = () => {
        launchImageLibrary({includeBase64 : true, quality: 0.5, maxHeight: 200, maxWidth: 200}, response => {
            if(response.didCancel || response.error) {
                showMessage({
                    message: 'kamu tidak memilih fotonya',
                    type: 'default',
                    backgroundColor: colors.error,
                    color: colors.text.third
                })
            } else {
                setPhotoForDB(`data:${response.type};base64, ${response.base64}`);
                const source = {uri: response.uri}
                setPhoto(source)
            }
        })
    }

    const rudi = () => {
        console.log('masuk')
    }
    return (
        <View style={styles.container}>
            <Header title="Edit Profile" onPress={() => navigation.goBack()}/>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.content}>
                    <Profile removePhoto photo={photo} onPress={getImage}/>
                    <Input label="fullName" value={profile.fullName} onChangeText={(value) => changeText('fullName', value)}/>
                    <Input label="profession" value={profile.profession} onChangeText={(value) => changeText('profession', value)} onPressIn={rudi} />
                    <Input label="email" value={profile.email} disable />
                    <Input label="password" secureTextEntry value={password} onChangeText={(value) => setPassword(value)}/>
                    <Button title="save profile" onPress={updateProfileData}/>
                </View>
            </ScrollView>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.background,
        flex: 1
    },
    content: {
        padding: 40,
        paddingTop: 0
    }
});

//make this component available to the app
export default EditProfile;
