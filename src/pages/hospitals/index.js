//import liraries
import React from 'react';
import { StyleSheet, Text, View, ImageBackground } from 'react-native';
import {DummyHospital1, DummyHospital2, DummyHospital3, ILHospitalBG} from '../../assets'
import ListHospitals from '../../components/listhospitals';
import { colors } from '../../utils/colors';

// create a component
const Hospitals = () => {
    return (
        <View style={styles.container}>
            <ImageBackground source={ILHospitalBG} style={styles.background}>
                <Text style={styles.title}>Nearby Hospitals</Text>
                <Text style={styles.desc}>3 Tersedia</Text>
            </ImageBackground>
            <View style={styles.content} >
                <ListHospitals type="Rumah Sakit" name="Citra Bunga Merdeka" address="jl. Surya Sejahtera 20" pic={DummyHospital1} />
                <ListHospitals type="Rumah Sakit Anak" name="Happy Family Kids" address="jl. Surya Sejahtera 20" pic={DummyHospital2} />
                <ListHospitals type="Rumah Sakit Jiwa" name="RSJ Cisarua" address="jl. Surya Sejahtera 20" pic={DummyHospital3} />
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    background: {
        height: 240,
        paddingTop: 30
    },
    title: { 
        fontSize: 20,
        fontWeight: '600',
        color: colors.text.third,
        textAlign: 'center'
    },
    desc: {
        fontSize: 14,
        fontWeight: '300',
        color: colors.text.third,
        textAlign: 'center',
        marginTop: 6
    },
    content: {
        flex: 1,
        backgroundColor: colors.background,
        borderRadius: 20,
        marginTop: -30,
        paddingTop: 14
    }
});

//make this component available to the app
export default Hospitals;
