//import liraries
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { ILNullPhoto } from '../../assets';
import { Header, List, Profile } from '../../components'
import { colors, getData } from '../../utils';
import { Fire } from '../../config';
import { showMessage } from 'react-native-flash-message';

// create a component
const UserProfile = ({navigation}) => {
    const [profile, setProfile] = useState({
        fullName: '',
        profession: '',
        photo: ILNullPhoto
    })
    useEffect(() => {
        getData('user').then(res => {
            const data = res;
            data.photo = {uri: res.photo}
            setProfile(data)
        })
    })
    const signOut = () => {
        Fire.auth().signOut().then( () => {
            navigation.replace('GetStarted')
        }).catch(err => {
            showMessage({
                message: err.message,
                type: 'default',
                backgroundColor: colors.error,
                color: colors.text.third
            })
        })
    }
    return (
        <View style={styles.container}>
            <Header title="Profile" onPress={() => navigation.goBack()}/>
            {profile.fullName.length > 0 && <Profile name={profile.fullName} desc={profile.profession} photo={profile.photo} />}    
            <List onPress={() => navigation.navigate('EditProfile')} name="Edit Profile" desc="Last Update Yesterday" type="next" icon="edit-profile" />
            <List name="Language" desc="Last Update Yesterday" type="next" icon="language" />
            <List name="Give Use Rate" desc="Last Update Yesterday" type="next" icon="rate" />
            <List name="Sing Out" desc="Keluar dari user" type="next" icon="help" onPress={signOut} />
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.background,
        flex: 1
    },
});

//make this component available to the app
export default UserProfile;
