//import liraries
import React, { Component, useEffect, useState } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { ChatItem, Header, InputChat } from '../../components';
import { colors, errorMessage, getData } from '../../utils';
import { getChatTIme, setDateChat } from '../../utils/date';
import NotifService from '../../NotifService';
import { Fire } from '../../config';

// create a component
const Chatting = ({navigation, route}) => {
    const [chatContent, setChatContent] = useState('');
    const [user, setUser] = useState({});
    const [chatData, setChatData] = useState([]) 
    const [registerToken, setRegisterToken] = useState('');
    const [fcmRegistered, setFcmRegistered] = useState(false);

    
  const onNotif = (notif) => {
    alert(notif.title, notif.message)
    Fire.messaging().getToken().then((currentToken) => {if (currentToken) {
        sendTokenToServer(currentToken);
        } 
        else {
        // Show permission request.
        RequestPermission();
            setTokenSentToServer(false);
        }
        }).catch((err) => {
            setTokenSentToServer(false);
        });
    }

    const onRegister = (token) => {
        setRegisterToken(token.token);
        setFcmRegistered(true);
    }

    const notif = new NotifService(onRegister, onNotif)

    const dataDoctor = route.params;
    useEffect(() => {
        getDataUser();
        const chatId = `${user.uid}_${dataDoctor.uid}`;        
        const urlFirebase = `chatting/${chatId}/allChat/`;

        Fire.database().ref(urlFirebase).on('value', (snapshot) => {
            if(snapshot.val()) {
                const dataSnapshot = snapshot.val();
                const allDataChat = [];
                Object.keys(dataSnapshot).map(key => {
                    const dataChat = dataSnapshot[key];
                    const newDataChat = [];

                    Object.keys(dataChat).map(itemChat => {
                         newDataChat.push({
                             id: itemChat,
                             data: dataChat[itemChat]
                         })
                    })
                    

                    allDataChat.push({
                        id: key,
                        data: newDataChat
                    })
                })
                setChatData(allDataChat);

                console.log(dataSnapshot)
            }
        })
    }, [dataDoctor.uid, user.uid])

    const getDataUser = () => {
        getData('user').then(res => {
            setUser(res)
        })
    }
    
    const chatSend = () => {
        const today = new Date();
        const chatId = `${user.uid}_${dataDoctor.uid}`;
        const urlFirebase = `chatting/${chatId}/allChat/${setDateChat(today)}`;
        const urlMessagesUser = `messages/${user.uid}/${chatId}`;
        const urlMessagesDoctor = `messages/${dataDoctor.uid}/${chatId}`;

        const data = {
            sendBy: user.uid,
            chatDate: today.getTime(),
            chatTime: getChatTIme(today),
            chatContent: chatContent
        }

        const dataHistoryChatUser = {
            lastChatContent: chatContent,
            lastChatDate: today.getTime(),
            uidPartner: dataDoctor.uid,
        }

        const dataHistoryChatDoctor = {
            lastChatContent: chatContent,
            lastChatDate: today.getTime(),
            uidPartner: user.uid,
        }

        Fire.database()
        .ref(urlFirebase)
        .push(data)
        .then(() => {
            setChatContent('')
            // Chat History User
            Fire.database().ref(urlMessagesUser).set(dataHistoryChatUser);
            // Chat History Doctor
            Fire.database().ref(urlMessagesDoctor).set(dataHistoryChatDoctor);
        })
        .catch(err => {
            errorMessage(err.message)
        })
    }

    return (
        <View style={styles.container}>
            <Header title={dataDoctor.fullName} desc={dataDoctor.profession} photo={{uri: dataDoctor.photo}} type="dark" profile="true" onPress={() => navigation.goBack()}/>
            <View style={styles.content}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {chatData.map(chat => {
                        return (
                            <View key={chat.id}>
                                <Text style={styles.chatDate}>{chat.id}</Text>
                                {chat.data.map(itemChat => {
                                    const isMe = itemChat.data.sendBy === user.uid;
                                    return (
                                        <ChatItem 
                                            key={itemChat.id} 
                                            text={itemChat.data.chatContent} 
                                            date={itemChat.data.chatTime} 
                                            me={isMe} 
                                            photo={isMe ? null : {uri : dataDoctor.photo}}
                                        />
                                    )
                                })}
                            </View>
                        )
                    })}                    
                </ScrollView>
            </View>
            <InputChat value={chatContent} onChangeText={value => setChatContent(value)} onButtonPress={chatSend} />
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.background,
        flex: 1
    },
    content: {
        flex: 1
    },
    chatDate: {
        fontSize: 11,
        color: colors.text.secondary,
        textAlign: 'center',
        paddingBottom: 20
    }
});

//make this component available to the app
export default Chatting;
