//import liraries
import React, { useState } from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { IconAddPhoto, IconRemovePhoto, ILNullPhoto } from '../../assets';
import { Button, Header, Link } from '../../components';
import { colors, storeData } from '../../utils';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import { Fire } from '../../config';

// create a component
const UploadPhoto = ({navigation, route}) => {
    const {fullName, profession, uid} = route.params;
    const [hasPhoto, setHasPhoto] = useState(false)
    const [photo, setPhoto] = useState(ILNullPhoto)
    const [photoForDB, setPhotoForDB] = useState('')
    const getImage = () => {
        launchImageLibrary({includeBase64 : true, quality: 0.5, maxHeight: 200, maxWidth: 200}, response => {
            if(response.didCancel || response.error) {
                showMessage({
                    message: 'kamu tidak memilih fotonya',
                    type: 'default',
                    backgroundColor: colors.error,
                    color: colors.text.third
                })
            } else {
                setPhotoForDB(`data:${response.type};base64, ${response.base64}`);
                const source = {uri: response.uri}
                setPhoto(source)
                setHasPhoto(true)
            }
        })
    }
    const uploadAndContinue = () => {
        Fire.database()
        .ref('users/' + uid + '/')
        .update({photo: photoForDB});

        const data = route.params
        data.photo = photoForDB

        storeData('user', data)

        navigation.replace('MainApp')
    }
    return (
        <View style={styles.container}>
            <Header title="Upload Photo" onPress={() => navigation.goBack()}/>
            <View style={styles.content}>
                <View style={styles.profile}>
                    <TouchableOpacity style={styles.avatarWrapper} onPress={getImage}>
                        <Image source={photo} style={styles.avatar} />
                        {hasPhoto ? <IconRemovePhoto style={styles.addPhoto} /> : <IconAddPhoto style={styles.addPhoto} />}
                    </TouchableOpacity>
                    <Text style={styles.name}>{fullName}</Text>
                    <Text style={styles.profession} >{profession}</Text>
                </View>
                <View>
                    <Button disable={!hasPhoto} title="Upload dan lanjut" onPress={uploadAndContinue} />
                    <View style={{height: 30}} />
                    <Link title="skip langkah ini" align="center" size={16} onPress={()=> navigation.replace('MainApp')} />
                </View>
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.background,
        flex: 1
    },
    content: {
        paddingHorizontal: 40,
        paddingBottom: 40,
        flex: 1,
        justifyContent: 'space-between'
    },
    profile: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center'
    },
    avatar: {
        width: 100,
        height: 100,
        borderRadius: 100/2
    }, 
    avatarWrapper: {
        width: 120,
        height: 120,
        borderWidth: 1, 
        borderColor: colors.border,
        borderRadius: 120/2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    addPhoto: {
        position: 'absolute',
        bottom: 8,
        right: 6
    },
    name: {
        fontSize: 24, 
        color: colors.text.primary,
        fontWeight: '600',
        textAlign: 'center'
    },
    profession: {
        fontSize: 18,
        textAlign: 'center',
        color: colors.text.secondary
    }
});

//make this component available to the app
export default UploadPhoto;
