//import liraries
import React, { Component, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { ILLogo } from '../../assets';
import { Fire } from '../../config';
import { colors } from '../../utils';


// create a component
const Splash = ({navigation}) => {
    useEffect(() => {
        const unsubscribe =  Fire.auth().onAuthStateChanged(user => {
            setTimeout(() => {
                if(user) {
                    navigation.replace('MainApp')
                }
                else {
                    navigation.replace('GetStarted')
                }
            }, 3000)
        })

        return () => unsubscribe();
    }, [navigation])

    return (
        <View style={styles.container}>
          <ILLogo />
          <Text style={styles.title}>App Doctor</Text>
        </View>
    );
};
    
    // define your styles
const styles = StyleSheet.create({
    container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.background,
    },
    title: {
        marginTop: 20,
        fontWeight: '600',
    }
});

//make this component available to the app
export default Splash;
