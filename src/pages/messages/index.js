//import liraries
import React, {useEffect, useState} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { List } from '../../components'
import { colors, getData } from '../../utils';
import { Fire } from '../../config';

// create a component
const Messages = ({navigation}) => {
    const [user, setUser] = useState({});
    const [historyChat, setHistoryChat] = useState([]);

    useEffect(() => {
        getDataUser()
        const urlHistory = `messages/${user.uid}`;
        Fire.database().ref(urlHistory).on('value', async snapshot => {
            if(snapshot.val()){
                const oldData = snapshot.val();
                const data = [];
                const promises = await Object.keys(oldData).map(async key => {
                    const urlUidDoctor = `doctors/${oldData[key].uidPartner}`;
                    const detailDoctor = await Fire.database().ref(urlUidDoctor).once('value');
                    data.push({
                        id: key,
                        detailDoctor : detailDoctor.val(),
                        ...oldData[key]
                    })
                })

                await Promise.all(promises)
                setHistoryChat(data);
            }
        })
    }, [user.uid])

    const getDataUser = () => {
        getData('user').then(res => {
            setUser(res)
        })
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title} >Messages</Text>
            {
                historyChat.map(chat => { 
                    const dataDoctor = {
                        id: chat.detailDoctor.uid,
                        ...chat.detailDoctor
                    }
                    return (
                        <List 
                            key={chat.id} 
                            profile={{uri : chat.detailDoctor.photo}} 
                            name={chat.detailDoctor.fullName} 
                            desc={chat.lastChatContent} 
                            onPress={() => navigation.navigate('Chatting', dataDoctor)}
                        />
                    )
                })
            }
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
    },
    title: {
        fontSize: 20,
        fontWeight: '600',
        color: colors.text.primary,
        marginTop: 30,
        marginLeft: 16
    }
});

//make this component available to the app
export default Messages;
