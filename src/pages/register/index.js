//import liraries
import React, { useState } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { Header, Input, Button, Loading } from '../../components';
import { colors, storeData, useForm } from '../../utils';
import { Fire } from '../../config';
import { showMessage } from "react-native-flash-message";

// create a component
const Register = ({navigation}) => {
    const [form, setForm] = useForm({
        fullName: '',
        profession: '',
        email: '',
        password: ''
    })

    const [loading, setLoading] = useState(false)

    const onContinue = () => {
        setLoading(true)
        Fire.auth().createUserWithEmailAndPassword(form.email, form. password)
        .then((user) => {
            setLoading(false)
            setForm('reset')
            const data = {
                fullName: form.fullName,
                profession: form.profession,
                email: form.email,
                uid: user.user.uid  
            }
            Fire.database().ref('users/' + user.user.uid + '/').set(data);
            storeData('user', data);
            navigation.navigate('UploadPhoto', data);
        })
        .catch((error) => {
            const errorMessage = error.message;
            setLoading(false)
            showMessage({
                message: errorMessage,
                type: "default",
                backgroundColor: colors.error,
                color: colors.text.third
            });
        });
    }
    return (
        <>
        <View style={styles.container}>
            <Header title="Daftar Akun" onPress={() => navigation.goBack()}/>
            <View style={styles.content}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Input label="Nama Lengkap" value={form.fullName} onChangeText={value => setForm('fullName', value)}/>
                    <Input label="Pekerjaan" value={form.profession} onChangeText={value => setForm('profession', value)}/>
                    <Input label="Alamat Email" value={form.email} onChangeText={value => setForm('email', value)}/>
                    <Input label="Password" value={form.password} onChangeText={value => setForm('password', value)} secureTextEntry/>
                    <Button title="Selanjutnya" onPress={onContinue} />
                </ScrollView>
            </View>
        </View>
        {loading && <Loading />}
        </>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.background,
    },
    content: {
        padding: 40, 
        paddingTop: 0,
    }
});

//make this component available to the app
export default Register;
