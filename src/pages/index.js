import Splash from './splash'
import GetStarted from './getstarted'
import Login from './login'
import Register from './register'
import UploadPhoto from './uploadphoto'
import Doctor from './doctor'
import Hospitals from './hospitals'
import Messages from './messages'
import SelectDoctor from './selectdoctor'
import Chatting from './chatting'
import UserProfile from './userprofile'
import EditProfile from './editprofile'
import DoctorProfile from './doctorprofile'


export {DoctorProfile, EditProfile, UserProfile, Splash, GetStarted, Login, Register, UploadPhoto, Doctor, Hospitals, Messages, SelectDoctor, Chatting}
