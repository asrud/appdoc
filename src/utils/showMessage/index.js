import { showMessage } from "react-native-flash-message"
import { colors } from "../colors"


export const errorMessage = (message) => {
    showMessage({
        message: message,
        type: "default",
        backgroundColor: colors.error,
        color: colors.text.third,
    })
}

export const successMessage = (message) => {
    showMessage({
        message: message,
        type: "default",
        backgroundColor: colors.primary,
        color: colors.text.third,
    })
}